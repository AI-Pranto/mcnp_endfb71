from multiprocessing import Pool
import glob
import openmc.data

neutron_files = glob.glob('*.tendl')

library = openmc.data.DataLibrary()

def process_neutron(path, temperatures=None):
    data = openmc.data.IncidentNeutron.from_njoy(path, temperatures=temperatures, stdout=True)

    h5_file = f'{data.name}.h5'
    print(f'Writing {h5_file} ...')
    data.export_to_hdf5(h5_file, 'w')

with Pool() as pool:
    results = []
    for filename in sorted(neutron_files):
        func_args = (filename, [600.0, 900.0, 1500.0])
        r = pool.apply_async(process_neutron, func_args)
        results.append(r)

    for r in results:
        r.wait()


# Register with library
for p in sorted(glob.glob('*.h5')):
    library.register_file(p)

# Write cross_sections.xml
library.export_to_xml('cross_sections.xml')
